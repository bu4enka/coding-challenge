/**

Print a pyramid!

    *
   ***
  *****
 *******
*********

ACCEPTANCE CRITERIA:

Write a script to output pyramid of given size to the console (with leading spaces).

*/

function pyramid(size = 5) {
  console.log("A beautiful pyramid")
  
  for (let i = 0; i < size; i++) {
    let row = '';

    for (let j = 1; j < size - i; j++) {
      row = row + ' ';
    }

    for (let k = 1; k <= 2 * i + 1; k++) {
      row = row + '*';
    }

    console.log(row);
  }
}


pyramid(5)
